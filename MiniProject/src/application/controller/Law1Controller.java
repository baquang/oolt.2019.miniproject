package application.controller;

import java.net.URL;
import java.util.ResourceBundle;

import application.model.CarObject;
import application.model.Force;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class Law1Controller {
	private double speed;
	private double onScreenSpeed;
//	CarObject obj = new CarObject();
//	Force force = new Force();
	TranslateTransition transition = new TranslateTransition();
	TranslateTransition transition2 = new TranslateTransition();
	ParallelTransition para;
	
    @FXML private ResourceBundle resources;

    @FXML private URL location;
    
    @FXML private AnchorPane law1AnchorPane;

    @FXML private TextField speedInput;

    @FXML private Button runBtn;

    @FXML private Button resetBtn;
    
    @FXML private Button stopBtn;
    
    @FXML private ImageView car;
    
    @FXML private ImageView road;
    
    @FXML private ImageView road2;
    
    @FXML
    void initialize() {
    	runBtn.setOnAction(this::runAction);
    	stopBtn.setOnAction(this::stopAction);
    	resetBtn.setOnAction(this::resetAction);
    }
    
    @FXML 
    private void runAction(ActionEvent event) {
    	if (para != null) para.stop();
    	try {
    		speed = Double.parseDouble(speedInput.getText());
    	} catch (Exception e) {
    		speed = 0;
    		speedInput.setText("");
    	}
    	
    	onScreenSpeed = speed*0.01;
    	transition.setNode(road);
    	transition2.setNode(road2);
    	
    	transition.setRate(onScreenSpeed);
    	transition2.setRate(onScreenSpeed);
    	
    	transition.setInterpolator(Interpolator.LINEAR);
    	transition2.setInterpolator(Interpolator.LINEAR);
    	
    	transition.setToX(-1280);
    	transition2.setToX(-1280);
    	
    	para = new ParallelTransition(transition,transition2);
    	para.setCycleCount(Animation.INDEFINITE);
    	para.play();
    	
    	if (speed == 0) para.stop();
    }
    
    @FXML
    private void stopAction(ActionEvent event) {
    	para.stop();
    }
    
    @FXML
    private void resetAction(ActionEvent event) {
    	stopAction(event);
    	speedInput.setText("");
    	road.setTranslateX(0);
    	road2.setTranslateX(0);
    	para.stop();
    }
}
