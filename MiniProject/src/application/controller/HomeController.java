package application.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

public class HomeController {

	@FXML private TabPane tabPane;
	
	@FXML private Tab law1;
	
	@FXML private Law1Controller law1Controller;

	@FXML private Tab law2;
	
	@FXML private Law2Controller law2Controller;
	
	@FXML private Tab law3;
	
	@FXML private Law3Controller law3Controller;
}
