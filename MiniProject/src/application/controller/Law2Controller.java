package application.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.animation.*;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class Law2Controller {
	float mass =0, force =0;
	float acceleration = 0;
	float velocity = 0, lastvelocity = 0;
	double local = 0, lastlocal = 0;
	double time = 1000;
	boolean flag = true;
    Timeline timeline = new Timeline();
	List<KeyFrame> key = new ArrayList<KeyFrame>();

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField massInput;

    @FXML
    private Button runButton;

    @FXML
    private TextField forceInput;

    @FXML
    private TextField aOutput;
   
    @FXML
    private Button resetButton;
   
    @FXML
    private ImageView car;
    
    @FXML
    void initialize() {
        
    	runButton.setOnAction(event ->{
        	flag = true;
    		try {
    		mass = (Float.parseFloat(massInput.getText()));
        	force = (Float.parseFloat(forceInput.getText()));
        	
    		}catch(Exception e) {
    			System.out.println("Error: You must input the value properly !!!");
    			flag = false;
    		}
    		
        	if(mass>0&&force>0) {
    			acceleration = force/mass;
    			aOutput.setText(Float.toString(acceleration)+" m/s^2");
        	}      
        	while(flag){
	        	lastvelocity = velocity;
	          	velocity = lastvelocity+acceleration;
	        	lastlocal =local;
	        	local += ((velocity*velocity - lastvelocity*lastvelocity)/(2*acceleration));
	        	
	        	System.out.println(lastlocal+" "+local+" "+acceleration+" "+time);
	        	if(lastlocal > 1500)
	        		break;
	        	key.add(new KeyFrame(Duration.millis(time),new KeyValue (car.xProperty(), local)));
	          	time+=1000;
	          	}
        	timeline.getKeyFrames().addAll(key);
          	timeline.play();
          	});
    
		resetButton.setOnAction (event ->{
			timeline.stop();
			mass = 0;
			force = 0;
			acceleration = 0;
			velocity = 0;
			local = 0;
			lastlocal = 0;
			lastvelocity = 0;
			massInput.setText("");
			forceInput.setText("");
			aOutput.setText("");
			car.setX(0);
			System.out.println("Clear "+key.size());
	  		key.clear();
		});
    }
}
