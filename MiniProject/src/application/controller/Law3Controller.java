package application.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

public class Law3Controller {
	TranslateTransition transition = new TranslateTransition();
	RotateTransition rotation = new RotateTransition();
	ParallelTransition roll;
	int flag = 0;
	
    @FXML private ResourceBundle resources;

    @FXML private URL location;
    
    @FXML private ImageView wall;
    
    @FXML private ImageView ball;
    
    @FXML private Button runBtn;
    
    @FXML private Button runHalfBtn;
    
    @FXML private Button stopBtn;
    
    @FXML private Button resetBtn;
    
    
    @FXML
    void initialize() {
    	runBtn.setOnAction(this::runAction);
    	runHalfBtn.setOnAction(this::runHalfAction);
    	stopBtn.setOnAction(this::stopAction);
    	resetBtn.setOnAction(this::resetAction);
    }
    
    private void runAction(ActionEvent event) {
    	resetAction(event);
    	transition.setNode(ball);
    	rotation.setNode(ball);
    	
    	rotation.setInterpolator(Interpolator.LINEAR);
    	rotation.setCycleCount(Animation.INDEFINITE);
    	rotation.setAutoReverse(true);
    	rotation.setByAngle(360);
    	rotation.setRate(0.1);
    	
    	transition.setInterpolator(Interpolator.LINEAR);
    	transition.setCycleCount(2);
    	transition.setAutoReverse(true);
    	transition.setToX(1194);
    	transition.setRate(0.1);
    	
    	roll = new ParallelTransition(transition, rotation);
    	transition.setOnFinished(finished -> roll.stop());
    	roll.play();
    }
    
    private void runHalfAction(ActionEvent event) {
    	
    	if (flag == 0) {
    		resetAction(event);
 
    		transition.setNode(ball);
    		rotation.setNode(ball);
    	
    		rotation.setInterpolator(Interpolator.LINEAR);
    		rotation.setCycleCount(Animation.INDEFINITE);
    		rotation.setByAngle(360);
    		rotation.setRate(0.1);
    		
    		transition.setInterpolator(Interpolator.LINEAR);
    		transition.setCycleCount(1);
    		transition.setToX(1194);
    		transition.setRate(0.1);
    		
    	} else {
    		rotation.setInterpolator(Interpolator.LINEAR);
        	rotation.setCycleCount(Animation.INDEFINITE);
        	rotation.setByAngle(-360);
        	rotation.setRate(0.1);
        	
        	transition.setInterpolator(Interpolator.LINEAR);
        	transition.setCycleCount(1);
        	transition.setToX(0);
        	transition.setRate(0.1);
        	
    	}
    	
    	roll = new ParallelTransition(transition, rotation);
		transition.setOnFinished(finished -> {
			roll.stop();
			if (flag == 0) {
				flag = 1;
			} else flag = 0;
		});
		roll.play();
    }	
    
    private void stopAction(ActionEvent event) {
    	if (roll != null) roll.stop();
    }
    
    private void resetAction(ActionEvent event) {
    	stopAction(event);
    	flag = 0;
    	ball.setRotate(0);
		ball.setTranslateX(0);
	}
}

	